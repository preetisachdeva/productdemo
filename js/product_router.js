"use strict";

window.APP = window.APP || {};
APP.ProductRouter = Backbone.Router.extend({
  routes: {
    "product/size": "size",
    "product/color": "color",
    "product/quantity": "quantity",
    "product/index": "index",
    "product/shipping/address": "shippingAddress",
    "product/detail": "productDetail"
  },

  $container: $('#primary-content'),

  initialize: function () {
    this.collection = new APP.ProductCollection();
    this.collection.fetch({ajaxSync: false});
    APP.helpers.debug(this.collection);
    this.index();
    // start backbone watching url changes
    Backbone.history.start();
  },
    index: function () {
        var view = new APP.NoteIndexView({collection: this.collection});
        this.$container.html(view.render().el);
    }, 

    size : function(){
        var view = new APP.ProductSizeView({
            collection: this.collection, 
            model: new APP.sizeModel()
        });
        this.$container.html(view.render().el);
    },
    color: function () {
        var view = new APP.ProductColorView({
            collection: this.collection, 
            model: new APP.ColorModel()
        });
        this.$container.html(view.render().el);
    },

    quantity: function () {
        var view = new APP.ProductQuantityView({
            collection: this.collection, 
            model: new APP.QuantityModel()
        });
        this.$container.html(view.render().el);
    },
    shippingAddress: function(){
        var view = new APP.ProductShippingAddressView({
            collection: this.collection, 
            model: new APP.ShippingAddressModel()
        });
        this.$container.html(view.render().el); 
    },
    productDetail : function(){
        var view = new APP.ProductDetailView({
            collection: this.collection
        });
        this.$container.html(view.render().el); 
    }
});
