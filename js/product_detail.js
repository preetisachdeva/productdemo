"use strict";
APP.QuantityModel = Backbone.Model.extend({
  
});

APP.ProductCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  localStorage: new Backbone.LocalStorage("ProductCollection"),
  model: APP.QuantityModel,
});
