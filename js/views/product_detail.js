"use strict";

APP.ProductDetailView = Backbone.View.extend({
    template: _.template($('#detailTemplate').html()),
    render: function () {
        this.$el.html(
        this.template({notes: this.collection.toJSON()})
        );
        return this;
    }
});
