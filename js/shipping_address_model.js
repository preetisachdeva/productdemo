"use strict";
APP.ShippingAddressModel = Backbone.Model.extend({
  // set  defaults variables
  defaults: {
    address1: "",
    address2: "",
    city: "",
    state: "",
    zip: "",
  },

  validate: function (attrs) {
    var errors = {};
    if (!attrs.address1) errors.address1 = "Please enter address first.";
    if (!attrs.address2) errors.address2 = "Please enter address secound.";
    if (!attrs.city) errors.city = "Please enter city";
    if (!attrs.state) errors.state = "Please enter state";
    if (!attrs.zip) errors.zip = "Please enter zip";
    if (!_.isEmpty(errors)) return errors;
  }
});

APP.NoteCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  localStorage: new Backbone.LocalStorage("ProductCollection"),
  model: APP.ShippingAddressModel
});
