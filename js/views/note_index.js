
"use strict";
APP.NoteIndexView = Backbone.View.extend({

	template: _.template($('#indexTemplate').html()),

  // populate the html to the dom
  render: function () {
  	console.log("collection",this.collection.toJSON());
    this.$el.html(
    	this.template({notes: this.collection.toJSON()})
    );
    return this;
  }
});

