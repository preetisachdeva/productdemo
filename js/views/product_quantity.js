"use strict";

APP.ProductQuantityView = Backbone.View.extend({
	// functions to fire on events
	events: {
		"click button.save": "save"
	},

	template: _.template($('#quantityTemplate').html()),

	initialize: function (options) {
		this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
	},

	save: function (event) {
		event.stopPropagation();
		event.preventDefault();
		this.model.set({
		   quantity: this.$el.find('#productquantity').val()
		});

		if (this.model.isValid()) {
		  this.collection.add(this.model);
		  this.model.save();
		  Backbone.history.navigate("product/shipping/address", {trigger: true});
		}
	},

	// populate the html to the dom
	render: function () {
		this.$el.html(
			this.template(this.model.toJSON())
		);
		return this;
	}
});
