"use strict";

APP.ProductShippingAddressView = Backbone.View.extend({
  // functions to fire on events
  events: {
    "click button.save": "save"
  },
  
  template: _.template($('#shippingFormTemplate').html()),

  initialize: function (options) {
    this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
  },

    save: function (event) {
        event.stopPropagation();
        event.preventDefault();

        // update our model with values from the form
        this.model.set({
          address1: this.$el.find('input[name=address1]').val(),
          address2: this.$el.find('input[name=address2]').val(),
          city: this.$el.find('input[name=city]').val(),
          state: this.$el.find('input[name=state]').val(),
          zip: this.$el.find('input[name=zip]').val()
        });
        
        if (this.model.isValid()) {
          this.collection.add(this.model);
          this.model.save();
          Backbone.history.navigate("product/detail", {trigger: true}); // redirect back to the index
        }
    },

    // populate the html to the dom
    render: function () {
        this.$el.html(
        	this.template(this.model.toJSON())
        );
        return this;
    }
});
