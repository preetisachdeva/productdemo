"use strict";
APP.sizeModel = Backbone.Model.extend({
  // set size defaults
  defaults: {
   size: ""
  },

  validate: function (attrs) {
    var errors = {};
    if (!attrs.size) errors.size = "Please select the value of size.";
    if (!_.isEmpty(errors)) return errors;
  }
});

APP.ProductCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  localStorage: new Backbone.LocalStorage("ProductCollection"),
  model: APP.sizeModel,
});
