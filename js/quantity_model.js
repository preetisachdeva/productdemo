"use strict";
APP.QuantityModel = Backbone.Model.extend({
  // set quantity defaults
  defaults: {
   quantity: ""
  },

  validate: function (attrs) {
     var errors = {};
     if (!attrs.quantity) errors.size = "Please select the value of quantity.";
     if (!_.isEmpty(errors)) return errors;
  }
});

APP.ProductCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  localStorage: new Backbone.LocalStorage("ProductCollection"),
  model: APP.QuantityModel,
});
