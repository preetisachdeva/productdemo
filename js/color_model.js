"use strict";
APP.ColorModel = Backbone.Model.extend({
  // set color defaults
  defaults: {
   color: ""
  },

  validate: function (attrs) {
     var errors = {};
     if (!attrs.color) errors.size = "Please select the color.";
     if (!_.isEmpty(errors)) return errors;
  }
});

APP.ProductCollection = Backbone.Collection.extend({
  // Reference to this collection's model.
  localStorage: new Backbone.LocalStorage("ProductCollection"),
  model: APP.ColorModel,
});
