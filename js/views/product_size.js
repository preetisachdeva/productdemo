"use strict";

APP.ProductSizeView = Backbone.View.extend({
  // functions to fire on events
    events: {
    	"click button.save": "save"
    },
  
  	template: _.template($('#productSizeTemplate').html()),

	initialize: function (options) {
		this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
	},

	save: function (event) {
		event.stopPropagation();
		event.preventDefault();
		// update our model with values from the form
		this.model.set({
		   size: this.$el.find('input[name=size]:checked').val()
		});

		if (this.model.isValid()) {
			this.collection.add(this.model);
			this.model.save();
			Backbone.history.navigate("product/color", {trigger: true}); // redirect back to the index
		}
	},

    // populate the html to the dom
    render: function () {
	    this.$el.html(
	    	this.template(this.model.toJSON())
	    );
    	return this;
  	}
});
