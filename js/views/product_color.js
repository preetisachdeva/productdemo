"use strict";

APP.ProductColorView = Backbone.View.extend({
    events: {
    "click button.save": "save",
    },
  
  template: _.template($('#colorTemplate').html()),

	initialize: function (options) {
		this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
	},

	save: function (event) {
		event.stopPropagation();
		event.preventDefault();
		this.model.set({
	    	color: this.$el.find('input[name=color]:checked').val()
		});

		if (this.model.isValid()) {
			this.collection.add(this.model);
			this.model.save();
			Backbone.history.navigate("product/quantity", {trigger: true});
		}
	},

	// populate the html to the dom
	render: function () {
	this.$el.html(
		this.template(this.model.toJSON())
	);
	return this;
	}
});
